####################################################################################################
# TWS API Connection/Auth
####################################################################################################

TWS_IP = "127.0.0.1"
TWS_Port = 7497
TWS_ClientID = 1

####################################################################################################
# Misc Behavior, Technicals
####################################################################################################

# If true, don't set up any orders, just say what we would do
DRY_RUN = True
# DRY_RUN = False

# Set in percent
unrealizedPNLmax = -1

# How often to check for term conditions
LOOP_INTERVAL = (40, 60)
