from setuptools import setup, find_packages

with open('README.md') as _f:
    _README_MD = _f.read()

_VERSION = '0.1'

setup(
    name='apptradestock',
    version=_VERSION,
    description='Apptradestock',
    long_description=_README_MD,
    classifiers=[
        "Typing :: Typed"],
    url='https://bitbucket.org/Nekrasovp/apptradestock/',
    packages=find_packages(include=['apptradestock*']),
    test_suite="test",
    setup_requires=[],
    tests_require=[],
    include_package_data=True,
    license='CC0 1.0',
    keywords='Stock Interactive Broker Trader Workstation'
)

