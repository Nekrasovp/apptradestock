python -m venv %CD%\venv
CALL %CD%\venv\Scripts\activate.bat
python -m pip install -r requirements.txt
If NOT exist "settings.py" (
 start copy _settings.py settings.py
)
