#!/usr/bin/env python
# encoding: utf-8
import time
import sys
import logging

from apptradestock.runner import Runner
import settings


def setup_custom_logger(name, log_level=logging.INFO) -> logging.Logger:
    formatter = logging.Formatter(fmt='%(asctime)s - '
                                        '%(name)s - '
                                        '%(funcName)s - '
                                        '%(message)s')

    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.setLevel(log_level)
    logger.addHandler(handler)
    return logger


def get_settings():
    """Build and return a dict of settings from imported settings module"""
    return {attr: getattr(settings, attr) for attr in dir(settings) if not callable(
        getattr(settings, attr)) and not attr.startswith("__")}


if __name__ == '__main__':
    start = time.time()
    # Init logger and define log_level verbosity
    logger = setup_custom_logger('ApptradeStock', log_level=logging.INFO)
    # Try/except just keeps ctrl-c from printing an ugly stacktrace
    try:
        Runner(logger=logger, **get_settings())
    except Exception:
        logger.info("Stop main loop for next reason:", exc_info=True)
    except (KeyboardInterrupt, SystemExit):
        logger.info(f'Shutdown application after {((time.time()-start)/60):.2f} minutes of working')
    sys.exit()
