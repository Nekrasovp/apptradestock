# ApptradeStock WPS 

ApptradeStock

## Installation

### Windows 10:
Next steps are specified for Windows Console(cmd).

Ensure you have git2.2 and python3.7 and higher installed.
```cmd
python -V
git --version
```
Clone the repository or download with web browser.
```cmd
git clone *.git
```
Activate virtual environments and install requirements
```cmd
cd apptradestock
python -m venv %CD%\venv
%CD%\venv\Scripts\activate.bat
python -m pip install -r requirements.txt
```
Create settings file 
```cmd
copy _settings.py settings.py
```
Now you have to ensure your venv is activated and run script
```cmd
%CD%\venv\Scripts\activate.bat
python run.py
```


## Usage

Ensure your venv is activated and run script
```cmd
%CD%\venv\Scripts\activate.bat
python run.py
```
or use 
```cmd
run.bat
```



[Trader Workstation Application Programming Interface](https://interactivebrokers.github.io/tws-api/index.html)

## Tests

You can run tests by running `run_tests` in the root, or by running `python -m unittest discover test`. 
Code coverage is enabled by default.

